## Домашнее задание

1. Реализовать серверную и клиентскую часть с использование gRPC
2. Ипользовать все типы взаимодействия (unary, client streaming, server streaming, bi-directional streaming)

В качестве примера можно взять сервис температуры


```
service TemperatureService{
  rpc GetRecordById (GetByIdRequest) returns (TemperatureRecordResponse);
  rpc GetAllByDeviceId(GetAllByDeviceIdRequest) returns (stream TemperatureRecordResponse);
  rpc Save (TemperatureRecordRequest) returns (TemperatureRecordResponse);
  rpc SaveAll (stream TemperatureRecordRequest) returns (stream TemperatureRecordResponse);
}
```


Клиента можно реализовать на любом языке программирования. 

Здесь можно найти список поддерживаемых языков и соответствующие туториалы

https://grpc.io/docs/languages/
