package com.orioninc.itis.grpc.client.impl;

import com.google.protobuf.ByteString;
import com.orioninc.itis.grpc.EmployeeServiceGrpc;
import com.orioninc.itis.grpc.Messages;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Metadata;
import io.grpc.stub.MetadataUtils;
import io.grpc.stub.StreamObserver;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class EmployeeServiceClient {
    public static void main(String[] args) throws IOException, InterruptedException {
        final ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 9000)
                .usePlaintext()
                .build();
//блокирующий клиент
        final EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingClient
                = EmployeeServiceGrpc.newBlockingStub(channel);

//        неблокир. клиет
        final EmployeeServiceGrpc.EmployeeServiceStub nonBlockingClient =
                EmployeeServiceGrpc.newStub(channel);


        switch (Integer.parseInt(args[0])) {
            case 1:
                getByBadgeNumber(blockingClient);
                break;
            case 2:
                sendMetadata(blockingClient);
                break;
            case 3:
                getAll(blockingClient);
                break;
            case 4:
                addPhoto(nonBlockingClient);
                break;
            case 5:
                saveAll(nonBlockingClient);
                break;
            case 6:
                save(blockingClient);
                break;
        }

        Thread.sleep(1000);
        channel.shutdown();
        channel.awaitTermination(1, TimeUnit.SECONDS);
    }

    private static void save(EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingClient) {
        final Messages.Employee employee = Messages.Employee.newBuilder()
                .setId(10)
                .setFirstName("Oleg")
                .setLastName("Olegov")
                .setBadgeNumber(987)
                .setRate(8.9f)
                .build();

        final Messages.EmployeeResponse employeeResponse = blockingClient.save(Messages.EmployeeRequest.newBuilder()
                .setEmployee(employee).build());

        System.out.println(employeeResponse.getEmployee());
    }

    private static void saveAll(EmployeeServiceGrpc.EmployeeServiceStub nonBlockingClient) throws InterruptedException {
        List<Messages.Employee> employees = new ArrayList<>();
        employees.add(Messages.Employee.newBuilder()
                .setBadgeNumber(555)
                .setId(4)
                .setFirstName("John")
                .setLastName("Smith")
                .setRate(2.1f)
                .build());

        employees.add(Messages.Employee.newBuilder()
                .setBadgeNumber(6666)
                .setId(5)
                .setFirstName("Lisa")
                .setLastName("Ivanova")
                .setRate(9.1f)
                .build());


        final StreamObserver<Messages.EmployeeRequest> stream = nonBlockingClient.saveAll(new StreamObserver<Messages.EmployeeResponse>() {
            @Override
            public void onNext(Messages.EmployeeResponse value) {
                System.out.println(value.getEmployee());
            }

            @Override
            public void onError(Throwable t) {

            }

            @Override
            public void onCompleted() {
                System.out.println("onCompleted");
            }
        });

        for (Messages.Employee employee : employees) {
            final Messages.EmployeeRequest employeeRequest = Messages.EmployeeRequest.newBuilder()
                    .setEmployee(employee)
                    .build();
            stream.onNext(employeeRequest);
            Thread.sleep(1000);
        }

        stream.onCompleted();
    }

    private static void addPhoto(EmployeeServiceGrpc.EmployeeServiceStub nonBlockingClient) throws IOException {
        final StreamObserver<Messages.AddPhotoRequest> stream = nonBlockingClient.addPhoto(new StreamObserver<Messages.AddPhotoResponse>() {
            @Override
            public void onNext(Messages.AddPhotoResponse value) {
                System.out.println(value.getIsOk());
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(t);
            }

            @Override
            public void onCompleted() {

            }
        });

        final InputStream inputStream = EmployeeServiceClient.class.getClassLoader().getResourceAsStream("nature.jpg");

        while (true) {
            byte[] data = new byte[64 * 1024];
            final int bytesRead = inputStream.read(data);


            if (bytesRead == -1) {
                break;
            }

            if (bytesRead < data.length) {
                byte[] newData = new byte[bytesRead];
                System.arraycopy(data, 0, newData, 0, bytesRead);
                data = newData;
            }

            final Messages.AddPhotoRequest addPhotoRequest = Messages.AddPhotoRequest.newBuilder()
                    .setData(ByteString.copyFrom(data))
                    .build();

            stream.onNext(addPhotoRequest);
        }

        stream.onCompleted();

    }

    private static void getAll(EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingClient) {
        final Iterator<Messages.EmployeeResponse> employeeResponseIterator =
                blockingClient.getAll(Messages.GetAllRequest.newBuilder().build());

        while (employeeResponseIterator.hasNext()) {
            System.out.println(employeeResponseIterator.next().getEmployee());
        }
    }

    private static void sendMetadata(EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingClient) {
        Metadata metadata = new Metadata();
        metadata.put(Metadata.Key.of("username", Metadata.ASCII_STRING_MARSHALLER), "ivan");
        metadata.put(Metadata.Key.of("password", Metadata.ASCII_STRING_MARSHALLER), "pass12345");

        final Messages.EmployeeResponse employeeResponse = blockingClient.withInterceptors(
                MetadataUtils.newAttachHeadersInterceptor(metadata)
        ).getByBadgeNumber(Messages.GetByBadgeNumberRequest.newBuilder()
                .setBadgeNumber(1234).build());
        System.out.println(employeeResponse.getEmployee());
    }

    private static void getByBadgeNumber(EmployeeServiceGrpc.EmployeeServiceBlockingStub blockingClient) {
        final Messages.EmployeeResponse employeeResponse = blockingClient.getByBadgeNumber(Messages.GetByBadgeNumberRequest.newBuilder()
                .setBadgeNumber(1234)
                .build());
        System.out.println(employeeResponse.getEmployee());
    }


}
