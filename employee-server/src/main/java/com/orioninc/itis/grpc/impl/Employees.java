package com.orioninc.itis.grpc.impl;

import com.orioninc.itis.grpc.Messages;

import java.util.ArrayList;

public class Employees extends ArrayList<Messages.Employee> {
    private static Employees employees;

    public static Employees getInstance() {
        if (employees == null) {
            employees = new Employees();
        }
        return employees;
    }

    private Employees() {
        this.add(Messages.Employee.newBuilder()
                .setBadgeNumber(1234)
                .setId(1)
                .setFirstName("Ivan")
                .setLastName("Ivanov")
                .setRate(1.1f)
                .build());

        this.add(Messages.Employee.newBuilder()
                .setBadgeNumber(4321)
                .setId(2)
                .setFirstName("Petr")
                .setLastName("Petrov")
                .setRate(4.2f)
                .build());

        this.add(Messages.Employee.newBuilder()
                .setBadgeNumber(1111)
                .setId(3)
                .setFirstName("Sergey")
                .setLastName("Sidorov")
                .setRate(4.2f)
                .build());
    }
}
