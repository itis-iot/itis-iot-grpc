package com.orioninc.itis.grpc.impl;

import com.google.protobuf.ByteString;
import com.orioninc.itis.grpc.EmployeeServiceGrpc;
import com.orioninc.itis.grpc.Messages;
import io.grpc.stub.StreamObserver;

public class EmployeeService extends EmployeeServiceGrpc.EmployeeServiceImplBase {
    @Override
    public void getByBadgeNumber(Messages.GetByBadgeNumberRequest request,
                                 StreamObserver<Messages.EmployeeResponse> responseObserver) {
        for (Messages.Employee employee : Employees.getInstance()) {
            if (employee.getBadgeNumber() == request.getBadgeNumber()) {
                final Messages.EmployeeResponse employeeResponse = Messages.EmployeeResponse
                        .newBuilder()
                        .setEmployee(employee)
                        .build();
                responseObserver.onNext(employeeResponse);
                responseObserver.onCompleted();
                return;
            }
        }
        responseObserver.onError(new Exception("Employee not found with badgeNumber " + request.getBadgeNumber()));
    }

    @Override
    public void getAll(Messages.GetAllRequest request,
                       StreamObserver<Messages.EmployeeResponse> responseObserver) {
        for (Messages.Employee employee : Employees.getInstance()) {
            final Messages.EmployeeResponse response = Messages.EmployeeResponse.newBuilder()
                    .setEmployee(employee)
                    .build();

            responseObserver.onNext(response);
        }
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<Messages.AddPhotoRequest> addPhoto(StreamObserver<Messages.AddPhotoResponse> responseObserver) {
        return new StreamObserver<Messages.AddPhotoRequest>() {
            private ByteString result;

            @Override
            public void onNext(Messages.AddPhotoRequest value) {
                if (result == null) {
                    result = value.getData();
                } else {
                    result = result.concat(value.getData());
                }
                System.out.println("Received message with " + value.getData().size() + " bytes");
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(t);
            }

            @Override
            public void onCompleted() {
                System.out.println("Total bytes received " + result.size());
                responseObserver.onNext(Messages.AddPhotoResponse.newBuilder()
                        .setIsOk(true)
                        .build());
                responseObserver.onCompleted();
            }
        };
    }


    @Override
    public StreamObserver<Messages.EmployeeRequest> saveAll(StreamObserver<Messages.EmployeeResponse> responseObserver) {
        return new StreamObserver<Messages.EmployeeRequest>() {
            @Override
            public void onNext(Messages.EmployeeRequest value) {
                Employees.getInstance().add(value.getEmployee());
                responseObserver.onNext(
                        Messages.EmployeeResponse.newBuilder().setEmployee(value.getEmployee()).build()
                );
            }

            @Override
            public void onError(Throwable t) {
                System.out.println(t);
            }

            @Override
            public void onCompleted() {
                System.out.println("onCompleted");
                for (Messages.Employee employee : Employees.getInstance()) {
                    System.out.println(employee);
                }
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public void save(Messages.EmployeeRequest request, StreamObserver<Messages.EmployeeResponse> responseObserver) {
        Employees.getInstance().add(request.getEmployee());
        responseObserver.onNext(Messages.EmployeeResponse.newBuilder()
                .setEmployee(request.getEmployee())
                .build());
        responseObserver.onCompleted();
    }
}
