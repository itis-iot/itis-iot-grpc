package com.orioninc.itis.grpc.impl;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.ServerInterceptors;
import io.grpc.ServerServiceDefinition;

import java.io.IOException;

public class EmployeeServer {

    private Server server;

    public static void main(String[] args) throws IOException, InterruptedException {
        EmployeeServer employeeServer = new EmployeeServer();
        employeeServer.start();
    }

    private void start() throws IOException, InterruptedException {
        final int port = 9000;

        EmployeeService employeeService = new EmployeeService();

        final ServerServiceDefinition serverServiceDefinition = ServerInterceptors.interceptForward(employeeService, new HeaderServerInterceptor());

        server = ServerBuilder.forPort(port)
                .addService(serverServiceDefinition)
                .build()
                .start();

        System.out.println("Listening on port " + port);

        server.awaitTermination();
    }
}
